//
//  Words+CoreDataProperties.swift
//  
//
//  Created by Konstantin Chukhas on 02.09.2018.
//
//

import Foundation
import CoreData


extension Words {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Words> {
        return NSFetchRequest<Words>(entityName: "Words")
    }

    @NSManaged public var sin: String?
    @NSManaged public var ex: String?
    @NSManaged public var thm: String?
    @NSManaged public var tnc: String?
    @NSManaged public var word: String?
    @NSManaged public var tnl: String?

}
