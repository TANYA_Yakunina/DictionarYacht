//
//  HeaderViewClass.swift
//  YachtDictionary.256Devs
//
//  Created by Игорь on 12.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import Foundation

import UIKit

class AccordionHeaderViewThems: UITableViewCell {
    
    
    static let qDefaultAccordionHeaderViewHeight: CGFloat = 44.0;
    static let qAccordionHeaderViewReuseIdentifier = "qAccordionHeaderViewReuseIdentifier";
    
    @IBOutlet weak var translationLb: UILabel!
    @IBOutlet weak var translationRusLb: UILabel!
    @IBOutlet weak var arrowIcon: UIImageView!
    @IBOutlet weak var speechuBtn: UIButton!
    
}
