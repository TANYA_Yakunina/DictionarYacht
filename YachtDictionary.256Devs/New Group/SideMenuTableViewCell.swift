//
//  SideMenuTableViewCell.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 07.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var label: UILabel!
    
}
