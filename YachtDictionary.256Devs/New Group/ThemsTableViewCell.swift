//
//  ThemsTableViewCell.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 17.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit

class ThemsTableViewCell: UITableViewCell {
    @IBOutlet weak var transcription: UILabel!
    @IBOutlet weak var someText: UILabel!
    @IBOutlet weak var sin: UILabel!
    @IBOutlet weak var theme: UILabel!
    @IBOutlet weak var someTextTranslator: UILabel!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sinHeight: NSLayoutConstraint!
    
    @IBOutlet weak var speechuLbl: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
