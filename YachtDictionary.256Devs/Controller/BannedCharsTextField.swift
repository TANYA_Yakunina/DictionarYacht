//
//  BannedCharsTextField.swift
//  YachtDictionary.256Devs
//
//  Created by 1 on 28.10.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit

class BannedCharsTextField: UITextField, UITextFieldDelegate {
    
    @IBInspectable var bannedChars: String = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
        autocorrectionType = .no
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard string.characters.count > 0 else {
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        return prospectiveText.doesNotContainCharactersIn(matchCharacters: bannedChars)
    }
    
}


extension String {
    
    // Возвращает true, если строка не имеет общих символов с символами соответствия.
    func doesNotContainCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet as CharacterSet) == nil
    }
    
}
