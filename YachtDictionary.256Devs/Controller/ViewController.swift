//
//  ViewController.swift
//  YachtDictionary.256Devs
//
//  Created by Kostyantin Chukhas on 22.08.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import CodableFirebase
import GoogleMobileAds

class ViewController: UIViewController, GADBannerViewDelegate {
    
    var bannerView: GADBannerView!
    var coreData:CDHandler!
    var ref: DatabaseReference!
    var words = [Words]()
    var newWords = [""]
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    func getContext()  -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.delegate = self
        // getFirebase()
        //        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        //        Reach().monitorReachabilityChanges()
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        addBannerView()

        
    }
    
//    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        // Add banner to view and add constraints as above.
//        addBannerView()
//    }
    
    private func addBannerView() {
        bannerView.adUnitID = "ca-app-pub-5935867547212251/2430479537"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *) {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }
    
    
    func updateUserInterface() {
        activity.startAnimating()
        
        if CheckInternet.Connection(){
            getFirebase()
            view.isUserInteractionEnabled = true
        }else{
            Alt()
            activity.startAnimating()
            view.isUserInteractionEnabled = false
        }
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func sideMenu(){
        if revealViewController() != nil {
            
            revealViewController().rearViewRevealWidth = 170
            
            sideMenuButton.addTarget(revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            //            mBtn.target = revealViewController()
            //            mBtn.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rearViewRevealWidth = 170
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    func getFirebase(){
        ref = Database.database().reference()
        ref.child("about").observeSingleEvent(of: .value, with: { (snapshot) in
            // print(snapshot)
            for snap in snapshot.children{
                let  snap = snap as? DataSnapshot
                let about = snap?.childSnapshot(forPath: "about").value as? String
                //print("About\(about)")
                self.label?.text = about
                //
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        ref.observe(.value) { (snap) in
            let value = snap.value as? NSDictionary
            var version = value?["version"] as? Int
            //print("version \(version!)")
            
            var versionInCoreDate:[NSManagedObject] = []
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Words")
            
            do {
                versionInCoreDate = try context.fetch(fetchRequest) as! [NSManagedObject]
            }
            catch{
                print("could not get data search")
            }
            var ver = Int()
            var vers = [Words]()
            vers = versionInCoreDate as! [Words]
            for t in 0..<vers.count {
                ver = Int(vers[t].version)
            }
            //print("sssssssssss version \(ver)")
            //            let name = managedObject.value(forKey: "name")
            
            if version! == ver { //проверка версии словаря
                self.activity.stopAnimating()
                //считываем обьект
                //                if CDHandler.fetchObject() != nil{
                //                    self.words = CDHandler.fetchObject()!
                //                    Firebase.GetFireBase()
                //
                //                }
            } else {
                CDHandler.cleanCoreData()
                self.ref = Database.database().reference()
                self.ref.observe(.value, with: { (snapVersion) in
                    let value = snapVersion.value as? NSDictionary
                    let version = value?["version"] as? Int
                    print("version2222 \(version!)")
                    
                })
                
                
                self.ref.child("words").observeSingleEvent(of: .value, with: { (snapshot) in
                    var vals = Array<String>()
                    var sins = Array<String>()
                    var exs = Array<String>()
                    var someTxt = Array<String>()
                    // Get user value
                    for snap in snapshot.children{
                        
                        let  snap = snap as? DataSnapshot
                        let word = snap?.key
                        //print(word)
                        
                        for snap2 in (snap?.children)!{
                            
                            let wrd = (snap2 as! DataSnapshot).childSnapshot(forPath: "word").value as? String
                            let desc = (snap2 as! DataSnapshot).childSnapshot(forPath: "tnc").value as? String
                            let transc = (snap2 as! DataSnapshot).childSnapshot(forPath: "tnl").value as? String
                            let theme = (snap2 as! DataSnapshot).childSnapshot(forPath: "thm").value as? String
                            let sinn = (snap2 as! DataSnapshot).childSnapshot(forPath: "sin").value as? String
                            let ex = (snap2 as! DataSnapshot).childSnapshot(forPath: "ex").value as? String
                            
                            for val in ((snap2 as! DataSnapshot).childSnapshot(forPath: "val").children){
                                let  val = val as? DataSnapshot
                                vals.append((val?.value as? String)!)
                            }
                            for sin in ((snap as! DataSnapshot).childSnapshot(forPath: "sin").children){
                                let  sin = sin as? DataSnapshot
                                sins.append((sin?.value as? String)!)
                            }
                            
                            // print("DICTIONARY\(dic.keys)")
                            for some in ((snap2 as! DataSnapshot).childSnapshot(forPath: "ex").children){
                                let  some = some as? DataSnapshot
                                someTxt.append((some?.key)!)
                                
                            }
                            vals.forEach({ (it) in
                                print("Value   = \(it)")
                            })
                            
                            sins.forEach({ (A) in
                                print("Sinonium  =  \(A)")
                            })
                            
                            //
                            //                        print("Word  = \(word!)")
                            //                        print("sin  = \(sinn)")
                            //                        print("Desc  = \(desc)")
                            //                        print("Transc  = \(transc)")
                            //                        print("Theme  = \(theme)")
                            ////////////////
                            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                            let entity = NSEntityDescription.entity(forEntityName: "Words", in: context)
                            //создаем обьект
                            let words = word
                            let tnl = transc
                            let thm = theme
                            let tnc = desc
                            let sin = sinn
                            
                            let managedObject = NSManagedObject(entity: entity!, insertInto: context)
                            managedObject.setValue(wrd, forKey: "word")
                            managedObject.setValue(tnl, forKey: "tnl")
                            managedObject.setValue(thm, forKey: "thm")
                            managedObject.setValue(tnc, forKey: "tnc")
                            managedObject.setValue(sin, forKey: "sin")
                            //let x : Int = version!
                            //let versionString = String(x)
                            //print(_persistCString)
                            
                            managedObject.setValue(version, forKey: "version")
                            //print("sss \(version!)")
                            
                            var primer = ""
                            var primer1 = ""
                            for ex in ((snap2 as! DataSnapshot).childSnapshot(forPath: "ex").children){
                                let  ex = ex as? DataSnapshot
                                primer = primer+"&<style>"+"html *"+"{"+"font-family: Helvetica !important;"+"font-family:Helvetica !important;"+"}</style>"+"<b><font family="+"Helvetica-Bold"+"><font size="+"4.5"+">"+"<font color="+"gray"+">"+(ex?.key)!+"</font></font></font></b>& - <font size="+"4.5"+">"+"<font color="+"gray"+">"+(ex?.value as? String)!+"</font></font><br>"
                                //primer = primer+"&"+(ex? .key)!+"&" + (ex?.value as? String)!
                                primer1 = primer1+"&"+(ex? .key)!+"&" + (ex?.value as? String)!
                                
                            }
                            
                            managedObject.setValue(primer, forKey: "ex")
                            
                            managedObject.setValue(primer1, forKey: "someText")
                            //                                                print("primer\(primer)")
                            //
                            //                                                print("primer1\(primer1)")
                            
                            let seq = zip(exs, someTxt)
                            let dict = Dictionary(seq, uniquingKeysWith:{return $1})
                            //                        for (key, value) in dict {
                            //                            print("key = \(key),value = \(value)")
                            //                               managedObject.setValue(key, forKey: "someText")
                            //                              managedObject.setValue(key, forKey: "ex")
                            //                        }
                            
                            
                            //                        someTxt.forEach({ (some) in
                            //                            print("Some   = \(some)")
                            //                            managedObject.setValue(some, forKey: "someText")
                            //
                            //                        })
                            //                        exs.forEach({ (ex) in
                            //                            print("Ex   = \(ex)")
                            //                              managedObject.setValue(ex, forKey: "ex")
                            //                        })
                            //
                            // Save the data to coredata
                            do {
                                try context.save()
                                //print("save is ok")
                                self.activity.stopAnimating()
                            } catch let error as NSError {
                                print("Error, \(error.localizedDescription)")
                            }
                        }
                    }
                    //todo
                })
                
            }
            
            
        }
        
        
    }
    
    func Alt()  {
        let alert = UIAlertController(title: "Для работы словаря необходимо подключение к Интернету", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //activity.startAnimating()
        if CheckInternet.Connection(){
            //               getFirebase()
            view.isUserInteractionEnabled = true
        }else{
            Alt()
            //                activity.startAnimating()
            view.isUserInteractionEnabled = false
        }
    }
    
    
    //    override func viewWillAppear(_ animated: Bool) {
    //
    //       // updateUserInterface()
    //
    //    }
}
