//
//  FirstViewController.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 12.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import CodableFirebase
import GoogleMobileAds

class FirstViewController: UIViewController, GADBannerViewDelegate {
    
    @IBOutlet weak var label: UILabel!
    // @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var textView: UITextView!
    
    var bannerView: GADBannerView!
    var coreData:CDHandler!
    var ref: DatabaseReference!
    var words = [Words]()
    var aboutText = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.delegate = self
//        8getFirebase()
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        addBannerView()
    }
    
    private func addBannerView() {
        bannerView.adUnitID = "ca-app-pub-5935867547212251/4353278759"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *) {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }
    
    func alert()  {
        // create the alert
        let alert = UIAlertController(title: "Для работы словаря необходимо подключение к Интернету", message: "Нет Интернета", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        //alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            print("Click of default button")
            self.view.isUserInteractionEnabled = true
            
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func updateUserInterface() {
        //        guard let status = Network.reachability?.status else { return }
        //        switch status {
        //        case .unreachable:
        //            //            view.backgroundColor = .red
        //            alert()
        //            self.view.isUserInteractionEnabled = false
        //            //self.activity.startAnimating()
        //        case .wifi:
        //            //            view.backgroundColor = .green
        //            //self.activity.stopAnimating()
        //            self.view.isUserInteractionEnabled = true
        //        case .wwan:
        //            // view.backgroundColor = .yellow
        //            //self.activity.stopAnimating()
        //            self.view.isUserInteractionEnabled = true
        //}
        print("Reachability Summary")
        // print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func getFirebase(){
        ref = Database.database().reference()
        ref.child("about").observeSingleEvent(of: .value, with: { (snapshot) in
            // print(snapshot)
            for snap in snapshot.children{
                let  snap = snap as? DataSnapshot
                let about = snap?.childSnapshot(forPath: "about").value as? String
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        ref.observe(.value) { (snap) in
            let value = snap.value as? NSDictionary
            let about = value?["about"] as! String
            self.aboutText = about
            //  print("about\(about!)")
            // self.aboutText.append(about!)
            //  print("aboutText\(self.aboutText)")
            //self.label?.text = about
            self.textView.dataDetectorTypes = .address
            self.textView.dataDetectorTypes = .link
            self.textView.text = self.aboutText
            self.textView!.text =  self.aboutText.replacingOccurrences(of: ".*<|>", with: "", options: .regularExpression)
        }
        
        
    }
    
}



