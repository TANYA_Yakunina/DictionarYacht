//
//  ThemeViewController.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 05.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import GoogleMobileAds


class ThemeViewController: UIViewController,UISearchControllerDelegate, GADBannerViewDelegate {
    
    
    
 //   @IBOutlet weak var label: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var itemName:[NSManagedObject] = []
    var activ = true
    @IBOutlet var popOver: UIView!
    //    var currentTabIndex = 0
   // @IBOutlet weak var activity: UIActivityIndicatorView!

    var bannerView: GADBannerView!
    var words = [Words]()
    var filteredWords = [Words]()
    var objects : [Words] = []
    var searchResult: [Words] = []
    var ref: DatabaseReference!
    var coreData:CDHandler!
    var dictClients = [String:String]()
    var arrayClient  = NSMutableArray()
    var storage : [String: [String]] = [:]
    var thems :[String] = []
    var newWords : [Words] = []
    
    var searchController:UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchRequestCoreData()
        sideMenu()
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.delegate = self
       // searchBar.delegate = self
        searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        addBannerView()
    }
    
    private func addBannerView() {
        bannerView.adUnitID = "ca-app-pub-5935867547212251/7475756808"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *) {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }
    
    func Alt()  {
        let alert = UIAlertController(title: "Для работы словаря необходимо подключение к Интернету", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //activity.startAnimating()
        if CheckInternet.Connection(){
            //               getFirebase()
            view.isUserInteractionEnabled = true
        }else{
            Alt()
            //                activity.startAnimating()
            view.isUserInteractionEnabled = false
        }
    }
    
    func updateUserInterface() {
       // activity.startAnimating()
        if CheckInternet.Connection(){
            //getFirebase()
            view.isUserInteractionEnabled = true
        }else{
            Alt()
        //    activity.startAnimating()
            view.isUserInteractionEnabled = false
        }
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    
    @IBAction func popBtn(_ sender: Any) {
        if activ{
            popOver.isHidden = false
        } else   {
            popOver.isHidden = true
            
        }
        activ = !activ
        
    }
    
    
    @IBAction func forwardBottonPopover(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.show(vc, sender: self)
        
    }
    @IBAction func backBottonPopover(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.show(vc, sender: self)
    }
    
    
    // MARK: - Menu
    
    func sideMenu(){
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 170
            sideMenuButton.addTarget(revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 170
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    ////////////Data/////////////////
    //MARK: FireBase
    
    //FireBase
    func fetchRequestCoreData(){
        // get the data from core data
        getData()
        //reload the table view
        tableView.reloadData()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Words", in: context)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Words")
        do{
            let sort1 = NSSortDescriptor(key: "thm", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
            
            fetchRequest.sortDescriptors = [sort1]
            fetchRequest.returnsDistinctResults = true
            
            
            itemName = try context.fetch(fetchRequest)
        }
        catch
        {
            print("Error in loading data")
        }
        
        var theme = ""
        var words = itemName as! [Words]
        var exist = false
        newWords.removeAll()
        for i in 0..<words.count {
            if newWords.count == 0 {
                newWords.append(words[i])
            } else {
                for ii in 0..<newWords.count {
                    print("i\(i)")
                    print("ii\(ii)")
                    if words[i].thm == newWords[ii].thm {
                        exist = true
                        break
                    }
                }
                if(!exist){
                    newWords.append(words[i])
                }else{
                    exist = !exist
                }
            }
        }
    }
    
    func getData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            itemName = try context.fetch(Words.fetchRequest())
            print("objeсcts\(objects)")
        }
        catch let error as NSError {
            print("Error, \(error.localizedDescription)")
        }
    }
    func filterStoreSearch(textField:String){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Words")
        // Add Sort Descriptor
        let sortDescriptor = NSSortDescriptor(key: "word", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let request: NSFetchRequest<Words> = Words.fetchRequest()
        
        let sort1 = NSSortDescriptor(key: "thm", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        let sort2 = NSSortDescriptor(key: "word", ascending: true)
        
        request.sortDescriptors = [sort1,sort2]
        // Add Predicate
        let predicate = NSPredicate(format: "word CONTAINS[c] %@", textField)
        fetchRequest.predicate = predicate
        print("Predicate\(predicate)")
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if let managedData = try? context.fetch(fetchRequest), let castedData = managedData as? [Words]  {
            for i in 0..<castedData.count {
                print(castedData[i].word ?? "")
            }
        }
    }
    
}

extension ThemeViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    
    func uniqueElementsFrom(array: [String]) -> [String] {
        //Create an empty Set to track unique items
        var set = Set<String>()
        let result = array.filter {
            guard !set.contains($0) else {
                //If the set already contains this object, return false
                //so we skip it
                return false
            }
            //Add this item to the set since it will now be in the array
            set.insert($0)
            //Return true so that filtered array will contain this item.
            return true
        }
        return result
    }
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newWords.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ThemTableViewCell
        let words = newWords[indexPath.row]
        cell.label?.text = words.value(forKey: "thm") as? String
        return cell
    }
    
    
    func tableView(_ tableView: UITableView,  indexPath: IndexPath) {
        let destination = ThemeViewController()
        navigationController?.pushViewController(destination, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        performSegue(withIdentifier: "segue", sender: newWords[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!)! as! ThemTableViewCell
        print(currentCell.textLabel!.text ?? "")
        let pr = segue.destination as! segueThemsViewController
        pr.valueThem = currentCell.label!.text!
        //pr.delegate = self"
        
        
    }
    
    
    
}




extension ThemeViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""
        {
            let escapedString = NSRegularExpression.escapedPattern(for: searchText)
            let pattern = ".*[ ]\(escapedString).*|^\(escapedString).*"
            var predicate:NSPredicate = NSPredicate()
            let engAlhabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
            if(engAlhabet.contains(searchText.first!)){
                //                predicate = NSPredicate(format:"word contains [c] '\(searchText)'")
                predicate = NSPredicate(format: "word MATCHES[c]'\(pattern)'")
            }else{
                predicate = NSPredicate(format:"tnl MATCHES[c]'\(pattern)'")
            }
            
            
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Words")
            fetchRequest.predicate = predicate
            // Add Sort Descriptor
            let sortDescriptor = NSSortDescriptor(key: "word", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            
            let request: NSFetchRequest<Words> = Words.fetchRequest()
            
            let sort1 = NSSortDescriptor(key: "thm", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
            let sort2 = NSSortDescriptor(key: "word", ascending: true)
            
            request.sortDescriptors = [sort1,sort2]
            do {
                itemName = try context.fetch(fetchRequest) as! [NSManagedObject]
            }
            catch{
                print("could not get data search")
            }
        }
        tableView.reloadData()
        
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.searchBar.endEditing(true)
        self.searchBar.endEditing(true)
    }
}
