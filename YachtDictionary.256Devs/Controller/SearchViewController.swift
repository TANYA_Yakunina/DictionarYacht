
//
//  SearchViewController.swift
//  YachtDictionary.256Devs
//
//  Created by Kostyantin Chukhas on 21.08.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import Firebase
import CoreData
import FZAccordionTableView
import AVFoundation
import Dispatch
import GoogleMobileAds

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate,UISearchDisplayDelegate, JNExpandableTableViewDelegate, JNExpandableTableViewDataSource, UISearchControllerDelegate,UISearchResultsUpdating, GADBannerViewDelegate{
    
    func updateSearchResults(for searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
        
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    var bannerView: GADBannerView!
    var searchController :UISearchController!
    var itemName:[NSManagedObject] = []
    var itemNameSin:[NSManagedObject] = []
    var itemName1:[NSManagedObject] = []
    static fileprivate let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
    
    var searchControllerIsActive = false
    var newWords : [Words] = []
    var newWords1 : [Words] = []
    var reduction : [Words] = []
    var unreduction : [Words] = []
    var word : [Words] = []
    var sin : [Words] = []
    var ex : [Words] = []
    
    var dictClients = [String:String]()
    var arrayClient  = NSMutableArray()
    var storage : [String: [String]] = [:]
    var words = [Words]()
    var filteredWords = [Words]()
    var objects : [Words] = []
    var searchResult: [Words] = []
    var ref: DatabaseReference!
    var coreData:CDHandler!
    var searchTextt = ""
    @IBOutlet weak var textField: CustomTextField!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var tableView: JNExpandableTableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    
    @IBOutlet weak var viewItem: UIView!
    var activ = true
    @IBOutlet var popOver: UIView!
    
    private class func getContext()  -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    @IBAction func popBtn(_ sender: Any) {
        
        if activ{
            popOver.isHidden = false
        } else   {
            popOver.isHidden = true
            
        }
        activ = !activ
        
    }
    @IBAction func themBottonPopover(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ThemeViewController") as! ThemeViewController
        self.show(vc, sender: self)
        
        
    }
    @IBAction func backBottonPopover(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.show(vc, sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //  getFirebase()
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.delegate = self
        sigevent()
        searchBar.delegate = self
        searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        self.searchBar.becomeFirstResponder()
        tableView.register(SearchTableViewCell.classForCoder(), forCellReuseIdentifier: SearchViewController.kTableViewCellReuseIdentifier)
        tableView.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        tableView.setContentOffset(.zero, animated: true)
        addBannerView()
    }
    
    private func addBannerView() {
        bannerView.adUnitID = "ca-app-pub-5935867547212251/2088888121"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *) {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }
    
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 1, section: 1)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func Alt()  {
        let alert = UIAlertController(title: "Для работы словаря необходимо подключение к Интернету", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //activity.startAnimating()
        if CheckInternet.Connection(){
            view.isUserInteractionEnabled = true
        }else{
            Alt()
            view.isUserInteractionEnabled = false
        }
    }
    
    func updateUserInterface() {
        //  activity.startAnimating()
        if CheckInternet.Connection(){
            view.isUserInteractionEnabled = true
            tableView.isUserInteractionEnabled = true
            
        }else{
            Alt()
            tableView.isUserInteractionEnabled = false
            
            view.isUserInteractionEnabled = false
        }
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    func tableView(_ tableView: JNExpandableTableView, canExpand indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: JNExpandableTableView, willExpand indexPath: IndexPath) {
        let headerView = self.tableView.cellForRow(at: indexPath) as? AccordionHeaderView
        headerView?.arrowIcon.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        self.searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: JNExpandableTableView, willCollapse indexPath: IndexPath) {
        let headerView = self.tableView.cellForRow(at: indexPath) as? AccordionHeaderView
        headerView?.arrowIcon.transform = CGAffineTransform(rotationAngle: CGFloat(2*Double.pi))
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  newWords.count
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.tableView.expandedContentIndexPath == indexPath {
            tableView.rowHeight = UITableViewAutomaticDimension
            return UITableViewAutomaticDimension
        } else {
            return 50
            
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive
        
    }
    var filtered:[Any] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchControllerIsActive {
            
            tableView.rowHeight = UITableViewAutomaticDimension
            return JNExpandableTableViewNumberOfRowsInSection(tableView as! JNExpandableTableView, section, 1);
            
        } else {
            tableView.rowHeight = UITableViewAutomaticDimension
            return JNExpandableTableViewNumberOfRowsInSection(tableView as! JNExpandableTableView, section, 0);
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let words = newWords[indexPath.section]
        var ex = words.value(forKey: "ex") as? String
        
        ex = ex?.replacingOccurrences(of: "|s|", with: "/")
        ex = ex?.replacingOccurrences(of: "|b|", with: ".")
        ex = ex?.replacingOccurrences(of: "|s|", with: "/")
        ex = ex?.replacingOccurrences(of: "|m|", with: "'")
        let exArray = ex?.split(separator: "&")
        let someText = words.someText
        let exArraySomeText = someText?.split(separator: "&")
        checkLackTranslation(arraySomeText: exArraySomeText!)
        let string = exArray?.joined(separator: "")
        let data =   string?.data(using: String.Encoding.unicode)!
        let attrStr = try? NSAttributedString(data: data!,
                                              options: [.documentType: NSAttributedString.DocumentType.html,
                                                        .characterEncoding: String.Encoding.utf8.rawValue],
                                              documentAttributes: nil)
        
        if self.tableView.expandedContentIndexPath == indexPath {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "labelCell") as! SearchTableViewCell
            
            let wordLbl = words.word
            
            cell.transcription?.text = wordLbl!.replacingOccurrences(of: "|s|", with: "/")
            
            let thmReplacing = "[\(words.tnc!)]"
            if words.tnc! == "-" {
                cell.descriptionHeight.constant = 0
            } else {
                cell.descriptionHeight.constant = 40
            }
            cell.theme?.text = thmReplacing.replacingOccurrences(of: "[-]", with: "")
            let sinReplacing = words.sin
            cell.sin?.text = sinReplacing!.replacingOccurrences(of: "|s|", with: "/")
            cell.sin?.sizeToFit()
            cell.someText?.sizeToFit()
            cell.someTextTranslator?.attributedText = attrStr
            cell.someTextTranslator?.numberOfLines = 0
            cell.speechuLbl?.addTarget(self, action: #selector(speechBtn), for: .touchUpInside)
            cell.speechuLbl?.tag = indexPath.section
            return cell
        } else {
            var left = ""
            var right = ""
            
            let cell = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![0] as! AccordionHeaderView
            for (index, element) in exArraySomeText!.enumerated() {
                if(element.contains(searchTextt.lowercased())){
                    if (index % 2 == 0) {
                        left = String(element)
                        right = String(exArraySomeText![index+1])
                    }else{
                        left = String(exArraySomeText![index-1])
                        right = String(element)
                    }
                }
            }
            
            if(left.isEmpty || right.isEmpty
                || (words.value(forKey: "word") as? String)!.lowercased().contains(searchTextt.lowercased())
                || (words.value(forKey: "tnl") as? String)!.lowercased().contains(searchTextt.lowercased())){
                left = (words.value(forKey: "word") as? String)!
                right = (words.value(forKey: "tnl") as? String)!
            }
            var wrd = left.replacingOccurrences(of: "|s|", with: "/")
            wrd = wrd.replacingOccurrences(of: "|b|", with: ".")
            wrd = wrd.replacingOccurrences(of: "|m|", with: "'")
            
            cell.translationLb?.text = wrd
            cell.translationRusLb?.text = "(\( words.value(forKey: "thm") as! String))"
            var sT = right.replacingOccurrences(of: "|s|", with: "/")
            sT = sT.replacingOccurrences(of: "|m|", with: "'")
            sT = sT.replacingOccurrences(of: "|b|", with: ".")
            cell.someText?.text = sT
            
            
            return cell
        }
        
    }
    
    func checkLackTranslation(arraySomeText: [String.SubSequence]) {
        for i in arraySomeText {
            print(i)
        }
    }
    
    @objc func speechBtn (sender:UIButton) {
        let words = newWords[sender.tag]
        
        let utterace = AVSpeechUtterance(string: (words.value(forKey: "word") as? String)!)
        
        utterace.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterace.rate = 0.5
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterace)
        
        
    }
    
    func sideMenu(){
        if revealViewController() != nil {
            
            revealViewController().rearViewRevealWidth = 170
            
            sideMenuButton.addTarget(revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 170
        }
    }
    
    func textFieldChanged(_ sender: Any) {
        tableView.reloadData()
    }
    ////////////Data/////////////////
    //MARK: FireBase
    
    
    func getData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            objects = try context.fetch(Words.fetchRequest())
            // print("objeсcts\(objects)")
        }
        catch let _ as NSError {
            // print("Error, \(error.localizedDescription)")
        }
    }
    func scrollToFirstRoww() {
        let indexPath = NSIndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let range = NSRange(location: 0, length: searchText.utf16.count)
        let regex = try! NSRegularExpression(pattern: "[0-9]")
        
        if searchText != "" {
            print(searchText)
            searchTextt = searchText
            searchControllerIsActive = true
            
            if searchText[0] <= "z" && regex.firstMatch(in: searchText, options: [], range: range) == nil {
                print(searchText)
                
                
                var predicateFull: NSPredicate = NSPredicate()
                var predicateBegin: NSPredicate = NSPredicate()
                var predicateCont: NSPredicate = NSPredicate()
                var predicateEx: NSPredicate = NSPredicate()
                
                predicateFull = NSPredicate(format: "word == %@", argumentArray: [searchText])
                predicateBegin = NSPredicate(format: "word BEGINSWITH[cd] %@", argumentArray: [searchText])
                predicateCont = NSPredicate(format: "word CONTAINS[cd] %@", argumentArray: [searchText])
                predicateEx = NSPredicate(format: "ex CONTAINS[cd] %@", argumentArray: [searchText])

                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                let managedObjectContext = appDelegate.persistentContainer.viewContext
                
                let fetchRequestWord = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
                let fetchRequestEx = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
                
                fetchRequestWord.predicate = predicateBegin
                fetchRequestWord.predicate = predicateCont
                fetchRequestEx.predicate = predicateEx
                
                let sortDescriptorWord = NSSortDescriptor(key: "word", ascending: true,
                                                           selector: #selector(NSString.caseInsensitiveCompare))
                let sortDescriptorEx = NSSortDescriptor(key: "ex", ascending: true,
                                                        selector: #selector(NSString.caseInsensitiveCompare))
                fetchRequestWord.sortDescriptors = [sortDescriptorWord]
                fetchRequestEx.sortDescriptors = [sortDescriptorEx]
                fetchRequestWord.fetchBatchSize = 15
                fetchRequestEx.fetchBatchSize = 15
                
                do {
                    itemName = try managedObjectContext.fetch(fetchRequestWord) as! [NSManagedObject]
                    itemNameSin = try managedObjectContext.fetch(fetchRequestEx) as! [NSManagedObject]
                    itemName.append(contentsOf: itemNameSin)
                }
                catch{
                    print("could not get data search")
                }
                
                
            } else {
                var predicateBegin: NSPredicate = NSPredicate()
                var predicateCont:NSPredicate = NSPredicate()
                var predicateEx:NSPredicate = NSPredicate()
                predicateBegin = NSPredicate(format: "tnl beginswith [cd] '\(searchText)'")
                predicateCont = NSPredicate(format: "tnl contains [cd] '\(searchText)'")
                predicateEx = NSPredicate(format: "ex contains [cd] '\(searchText)' ")
                
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                let managedObjectContext = appDelegate.persistentContainer.viewContext
                
                let fetchRequestBegin = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
                let fetchRequestCont = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
                let fetchRequestEx = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
                
                let sortDescriptorBegin = NSSortDescriptor(key: "tnl", ascending: true,
                                                           selector: #selector(NSString.caseInsensitiveCompare))
                let sortDescriptorCont = NSSortDescriptor(key: "tnl", ascending: true,
                                                          selector: #selector(NSString.caseInsensitiveCompare))
                let sortDescriptorEx = NSSortDescriptor(key: "ex", ascending: true,
                                                        selector: #selector(NSString.caseInsensitiveCompare))
                
                fetchRequestBegin.sortDescriptors = [sortDescriptorBegin]
                fetchRequestCont.sortDescriptors = [sortDescriptorCont]
                fetchRequestEx.sortDescriptors = [sortDescriptorEx]
                
                fetchRequestBegin.predicate = predicateBegin
                fetchRequestCont.predicate = predicateCont
                fetchRequestEx.predicate = predicateEx
                
                fetchRequestBegin.fetchBatchSize = 15
                fetchRequestCont.fetchBatchSize = 15
                fetchRequestEx.fetchBatchSize = 15
                
                do {
                    itemName = try managedObjectContext.fetch(fetchRequestBegin) as! [NSManagedObject]
                    itemName1 = try managedObjectContext.fetch(fetchRequestCont) as! [NSManagedObject]
                    
                    itemName.append(contentsOf: itemName1)
                    
                    itemNameSin = try managedObjectContext.fetch(fetchRequestEx) as! [NSManagedObject]
                    
                    itemName.append(contentsOf: itemNameSin)
                }
                catch{
                    print("could not get data search")
                }
            }
            
            let words = itemName as! [Words]
            newWords.removeAll()
            newWords = words
            newWords = newWords.filterDuplicate{ ($0.objectID) }
            for t in 0..<newWords.count {
                if newWords[t].thm?.lowercased() == "сокращения" {
                    newWords[t].id = 0
                } else {
                    newWords[t].id = 1
                }
            }
            newWords = newWords.sorted(by:{$1.id < $0.id})
            tableView.reloadData()
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        tableView.beginUpdates()
        tableView.setContentOffset(CGPoint.zero, animated: true)
        tableView.endUpdates()
        return true
    }
    
    // MARK: - <FZAccordionTableViewDelegate> -
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        return true
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Helvetica-Bold", size: 14)!]
        let boldString = NSMutableAttributedString(string:text , attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Helvetica", size: 14)!]
        let normal = NSAttributedString(string: text, attributes: attrs)
        append(normal)
        
        return self
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
extension StringProtocol {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}
extension UITableViewCell {
    var tableView: UITableView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UITableView.self) == false) {
            view = view!.superview
        }
        return view as? UITableView
    }
}

extension Array
{
    func filterDuplicate<T>(_ keyValue:(Element)->T) -> [Element]
    {
        var uniqueKeys = Set<String>()
        return filter{uniqueKeys.insert("\(keyValue($0))").inserted}
    }
}
extension Int: Sequence {
    public func makeIterator() -> CountableRange<Int>.Iterator {
        return (0..<self).makeIterator()
    }
}

extension String {
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
}

