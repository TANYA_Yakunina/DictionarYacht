//
//  segueThemsViewController.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 13.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//


import CoreData
import Firebase
import FZAccordionTableView
import AVFoundation
import GoogleMobileAds

protocol DelegateVC {
    func sendThem(with name:String)
}

class segueThemsViewController: UIViewController, JNExpandableTableViewDelegate, JNExpandableTableViewDataSource,UISearchControllerDelegate, GADBannerViewDelegate {
    static fileprivate let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
    @IBOutlet weak var activity: UIActivityIndicatorView!

    @IBOutlet var popOver: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var tableView: JNExpandableTableView!
    
    var bannerView: GADBannerView!
    var valueThem = ""
    var currentTabIndex = 0
    var dictClients = [String:String]()
    var arrayClient  = NSMutableArray()
    var storage : [String: [String]] = [:]
    var words = [Words]()
    var filteredWords = [Words]()
    var objects : [Words] = []
    var searchResult: [Words] = []
    var newWords : [Words] = []
    var delegate :DelegateVC?
    var itemName:[NSManagedObject] = []
    var itemNameSin:[NSManagedObject] = []
    var itemName1:[NSManagedObject] = []
    var ref: DatabaseReference!
    var coreData:CDHandler!
    var activ = true
    var searchTextt = ""
    
    private class func getContext()  -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    var searchController:UISearchController!
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.delegate = self
        searchBar.delegate = self
        fetchRequestCoreData()
        searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        self.searchBar.becomeFirstResponder()
        
        tableView.register(SearchTableViewCell.classForCoder(), forCellReuseIdentifier: segueThemsViewController.kTableViewCellReuseIdentifier)
        tableView.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        //print(valueThem)
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        addBannerView()
    }
    
    private func addBannerView() {
        bannerView.adUnitID = "ca-app-pub-5935867547212251/2740814180"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *) {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }
    
    func Alt()  {
        let alert = UIAlertController(title: "Для работы словаря необходимо подключение к Интернету", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //activity.startAnimating()
        if CheckInternet.Connection(){
            //               getFirebase()
            view.isUserInteractionEnabled = true
        }else{
            Alt()
            //                activity.startAnimating()
            view.isUserInteractionEnabled = false
        }
    }
    
    func updateUserInterface() {
      //  activity.startAnimating()
        if CheckInternet.Connection(){
            //getFirebase()
            view.isUserInteractionEnabled = true
        }else{
            Alt()
        //    activity.startAnimating()
            view.isUserInteractionEnabled = false
        }
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    
    @IBAction func popBtn(_ sender: Any) {
        if activ{
            popOver.isHidden = false
        } else   {
            popOver.isHidden = true
            
        }
        activ = !activ
        
    }
    @IBAction func forwardBottonPopover(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.show(vc, sender: self)
        
    }
    @IBAction func backBottonPopover(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ThemeViewController") as! ThemeViewController
        self.show(vc, sender: self)
    }
    
    ////////////Data/////////////////
    //MARK:CoreData
    
    func fetchRequestCoreData(){
        // get the data from core data
        // getData()
        //reload the table view
        tableView.reloadData()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Words", in: context)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Words")
        let sortDescriptor = NSSortDescriptor(key: "word", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let request: NSFetchRequest<Words> = Words.fetchRequest()
        
        let sort1 = NSSortDescriptor(key: "thm", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        let sort2 = NSSortDescriptor(key: "word", ascending: true)
        
        request.sortDescriptors = [sort1,sort2]
        // Add Predicate
        var predicate = NSPredicate(format: "thm CONTAINS[c] %@", valueThem)
        //         let predicate = NSPredicate(format: "thm =[c] %@", textField)
        
        do{
            let sort1 = NSSortDescriptor(key: "thm", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
            
            fetchRequest.sortDescriptors = [sort1]
            fetchRequest.returnsDistinctResults = true
            
            
            itemName = try context.fetch(fetchRequest)
        }
        catch
        {
            print("Error in loading data")
        }
        
        var words = itemName as! [Words]
        // var exist = false
        newWords.removeAll()
        for k in 0..<words.count{
            if(words[k].thm == valueThem){
                newWords.append(words[k])
            }
        }
        
        
    }
    //    func getFirebase(){
    //        ref = Database.database().reference()
    //        ref.child("words").observeSingleEvent(of: .value, with: { (snapshot) in
    //            var vals = Array<String>()
    //            var sins = Array<String>()
    //            var exs = Array<String>()
    //            // Get user value
    //            for snap in snapshot.children{
    //                let  snap = snap as? DataSnapshot
    //                let word = snap?.key
    //                let desc = snap?.childSnapshot(forPath: "tnc").value as? String
    //                let transc = snap?.childSnapshot(forPath: "tnl").value as? String
    //                let theme = snap?.childSnapshot(forPath: "thm").value as? String
    //                let sinn = snap?.childSnapshot(forPath: "sin").value as? String
    //                let ex = snap?.childSnapshot(forPath: "ex").value as? String
    //                for val in (snap?.childSnapshot(forPath: "val").children)!{
    //                    let  val = val as? DataSnapshot
    //                    vals.append((val?.value as? String)!)
    //                }
    //                for sin in (snap?.childSnapshot(forPath: "sin").children)!{
    //                    let  sin = sin as? DataSnapshot
    //                    sins.append((sin?.value as? String)!)
    //                }
    //                for ex in (snap?.childSnapshot(forPath: "ex").children)!{
    //                    let  ex = ex as? DataSnapshot
    //                    exs.append((ex?.value as? String)!)
    //                }
    //                vals.forEach({ (it) in
    //                    print("Value   = \(it)")
    //                })
    //                sins.forEach({ (A) in
    //                    print("Sinonium  =  \(A)")
    //                })
    //                exs.forEach({ (ex) in
    //                    print("Ex   = \(ex)")
    //                })
    //                print("Word  = \(word!)")
    //                print("sin  = \(sinn)")
    //                print("Desc  = \(desc)")
    //                print("Transc  = \(transc)")
    //                print("Theme  = \(theme)")
    //
    //
    //
    //
    //                //////////////////
    //                //                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //                //                let entity = NSEntityDescription.entity(forEntityName: "Words", in: context)
    //                //                //создаем обьект
    //                //                let words = word
    //                //                let tnl = transc
    //                //                let thm = theme
    //                //                let tnc = desc
    //                //                let sin = sinn
    //                //                let managedObject = NSManagedObject(entity: entity!, insertInto: context)
    //                //                managedObject.setValue(words, forKey: "word")
    //                //                managedObject.setValue(tnl, forKey: "tnl")
    //                //                managedObject.setValue(thm, forKey: "thm")
    //                //                managedObject.setValue(tnc, forKey: "tnc")
    //                //                managedObject.setValue(sin, forKey: "sin")
    //                //                // managedObject.setValue(ex, forKey: "ex")
    //                //                // Save the data to coredata
    //                //                do {
    //                //                    try context.save()
    //                //                    print("save is ok")
    //                //                } catch let error as NSError {
    //                //                    print("Error, \(error.localizedDescription)")
    //                //                }
    //            }
    //            //todo
    //        }) { (error) in
    //            print(error.localizedDescription)
    //        }
    //    }
    
    func getData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            itemName = try context.fetch(Words.fetchRequest())
            print("objeсcts\(newWords)")
        }
        catch let error as NSError {
            print("Error, \(error.localizedDescription)")
        }
    }
    
}
extension segueThemsViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return  newWords.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.tableView.expandedContentIndexPath == indexPath {
            return UITableViewAutomaticDimension
        } else {
            return 47
            
        }
    }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        //проверка пустая ли строка или нет
    //        //return sections[section].name.count
    //        return newWords.count
    //    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JNExpandableTableViewNumberOfRowsInSection(tableView as! JNExpandableTableView, section, 1);
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let words = newWords[indexPath.section]
        var ex = words.value(forKey: "ex") as? String
        ex = ex?.replacingOccurrences(of: "|s|", with: "/")
        var exArray = ex?.split(separator: "&")
        
        var someText = words.value(forKey: "someText") as? String
        someText = someText?.replacingOccurrences(of: "|s|", with: "/")
        //       ex = ex?.replacingOccurrences(of: "|n|", with: "\n")
        let exArraySomeText = someText?.split(separator: "&")
        let string = exArray?.joined(separator: "")
        let data =   string?.data(using: String.Encoding.unicode)!
        let attrStr = try? NSAttributedString(data: data!,options:[NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, ],documentAttributes: nil)
        
        
        
        if self.tableView.expandedContentIndexPath == indexPath {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "labelCell") as! ThemsTableViewCell
            
            
            
//            cell.transcription?.text = words.value(forKey: "word") as? String
            let wordLbl = words.value(forKey: "word") as? String
            //            cell.transcription?.text = words.value(forKey: "word") as? String
            cell.transcription?.text = wordLbl!.replacingOccurrences(of: "|s|", with: "/")
            
            let thmReplacing = "[\(words.tnc!)]"
            if words.tnc! == "-" {
                cell.descriptionHeight.constant = 0
            } else {
                cell.descriptionHeight.constant = 40
            }
//            cell.theme?.text = "["+((words.value(forKey: "tnc") as? String)! )+"]"
            cell.theme?.text = thmReplacing.replacingOccurrences(of: "[-]", with: "")
//            cell.sin?.text = words.value(forKey: "sin") as? String
            let sinReplacing = words.value(forKey: "sin") as? String
            cell.sin?.text = sinReplacing!.replacingOccurrences(of: "|s|", with: "/")
            cell.someText?.text = (words.value(forKey: "someText") as? String)
            cell.someTextTranslator?.attributedText = attrStr
            cell.someTextTranslator?.numberOfLines = 0
            cell.speechuLbl.addTarget(self, action: #selector(speechBtn), for: .touchUpInside)
            cell.speechuLbl.tag = indexPath.section
            // cell.transcription?.text = sections[indexPath.section].transcription[indexPath.row]
            return cell
        } else {
            var left = ""
            var right = ""
            
            let cell = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![0] as! AccordionHeaderView
            // let words = itemName[indexPath.section]
            
            for (index, element) in exArraySomeText!.enumerated() {
                if(element.contains(searchTextt.lowercased())){
                    if (index % 2 == 0) {
                        left = String(element)
                        right = String(exArraySomeText![index+1])
                    }else{
                        left = String(exArraySomeText![index-1])
                        right = String(element)
                    }
                }
            }
            if(left.isEmpty || right.isEmpty
                || (words.value(forKey: "word") as? String)!.lowercased().contains(searchTextt.lowercased())
                || (words.value(forKey: "tnl") as? String)!.lowercased().contains(searchTextt.lowercased())){
                left = (words.value(forKey: "word") as? String)!
                right = (words.value(forKey: "tnl") as? String)!
            }
            
            cell.translationLb.text = left.replacingOccurrences(of: "|s|", with: "/")
            cell.translationRusLb.text = "(\( words.value(forKey: "thm") as! String))"
            cell.someText?.text = right.replacingOccurrences(of: "|s|", with: "/")
            
            return cell
            
        }
    }
    @objc func speechBtn (sender:UIButton) {
        let words = newWords[sender.tag]
        
        print(sender.tag)
        
        let utterace = AVSpeechUtterance(string: (words.value(forKey: "word") as? String)!)
        
        
        utterace.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterace.rate = 0.5
        
        
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterace)
        
        
    }
    
    //==========
    
    func tableView(_ tableView: JNExpandableTableView, canExpand indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: JNExpandableTableView, willExpand indexPath: IndexPath) {
        let headerView = self.tableView.cellForRow(at: indexPath) as! AccordionHeaderView
        headerView.arrowIcon.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        self.searchBar.resignFirstResponder()
        
    }
    
    func tableView(_ tableView: JNExpandableTableView, willCollapse indexPath: IndexPath) {
        print("willCollapse")
        let headerView = self.tableView.cellForRow(at: indexPath) as? AccordionHeaderView
        headerView?.arrowIcon.transform = CGAffineTransform(rotationAngle: CGFloat(2*Double.pi))
    }
    
    //---------
    
    // MARK: - <FZAccordionTableViewDelegate> -
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        return true
    }
    
    
    
}



extension segueThemsViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            
           // searchControllerIsActive = true
            var predicate: NSPredicate = NSPredicate()
            var predicateEx:NSPredicate = NSPredicate()
            var predicateSin:NSPredicate = NSPredicate()
            
            
            predicate = NSPredicate(format: "word contains[c] '\(searchText)' OR sin contains[c] '\(searchText)' AND thm MATCHES[c]'\(valueThem)'")
            predicateEx = NSPredicate(format: "ex contains[c] '\(searchText)'AND thm MATCHES[c]'\(valueThem)'")
            predicateSin = NSPredicate(format: "sin contains[c] '\(searchText)'AND thm MATCHES[c]'\(valueThem)'")
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedObjectContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
            //            let fetchRequest1 = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
            //            let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName:"Words")
            
            //let sortDescriptor1 = NSSortDescriptor(key: "word", ascending: true)
            let sortDescriptor1 = NSSortDescriptor(key: "word", ascending: true,
                                                   selector: #selector(NSString.caseInsensitiveCompare))
            //let sortDescriptor2 = NSSortDescriptor(key: "sin", ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor1]
            
            fetchRequest.predicate = predicateEx
            
            fetchRequest.predicate = predicateSin
            
            fetchRequest.predicate = predicate
            
            do {
                itemName = try managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]
                
                //                itemNameSin = try managedObjectContext.fetch(fetchRequest1) as! [NSManagedObject]
                //                   itemName = try managedObjectContext.fetch(fetchRequest2) as! [NSManagedObject]
                //
                //                itemNameSin.forEach { (item1) in
                //                    itemName1.append(item1)
                //                }
                //
                //                itemName.forEach { (item) in
                //                    itemName1.append(item)
                //                }
                //
                
                
                
            } catch let error as NSError {
                print("Could not fetch. \(error)")
            }
        }
        let  words = itemName as! [Words]
        
        //Удаление повторяющегося элемента из массива
        newWords.removeAll()
        newWords = words
//        newWords = newWords.filterDuplicate{ ($0.objectID) }
//        for t in 0..<newWords.count {
//            if newWords[t].thm?.lowercased() == "сокращения" {
//                newWords[t].id = 0
//                //print(newWords[t].sin)
//            } else {
//                newWords[t].id = 1
//                //print(newWords[t].sin)
//
//            }
//        }
      //  var words = itemName as! [Words]
                   var exist = false
                    newWords.removeAll()
                    for i in 0..<words.count{
                        if(words[i].thm == valueThem){
                            newWords.append(words[i])
                        }else {
                            for ii in 0..<newWords.count {
                                //                        print("i\(i)")
                                //                        print("ii\(ii)")
                                if words[i].objectID == newWords[ii].objectID {
                                    exist = true
                                    break
                                }
                            }
                        }
                    }
        newWords = newWords.sorted(by:{$1.id < $0.id})
        tableView.reloadData()
        
    }
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchText != ""
//        {
//            searchTextt = searchText
//            let escapedString = NSRegularExpression.escapedPattern(for: searchText)
//            let pattern = ".*[ ]\(escapedString).*|^\(escapedString).*"
//            let pattern1 = ".*[ ]\(escapedString).*|^\(escapedString).*"
//            let patternsin = ".*\(escapedString).*|^\(escapedString)"
//            
//            
//            var predicate:NSPredicate = NSPredicate()
//            var predicate1:NSPredicate = NSPredicate()
//            var predicatesin:NSPredicate = NSPredicate()
//            
//            let engAlhabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-/:;()₽&@«.,?!’[]{}#%^*+=_|~<>$€£•.,?!’"
//            let rusAlphabet = "А а, Б б, В в, Г г, Д д, Е е, Ё ё, Ж ж, З з, И и, Й й, К к, Л л, М м, Н н, О о, П п, Р р, С с, Т т, У у, Ф ф, Х х, Ц ц, Ч ч, Ш ш, Щ щ, Ъ ъ, Ы ы, Ь ь, Э э, Ю ю, Я я 1234567890-/:;()₽&@«.,?!’[]{}#%^*+=_|~<>$€£•.,?!’"
//            if(engAlhabet.contains(searchText.first!)){
//                //                predicate = NSPredicate(format:"word contains [c] '\(searchText)'")
//                predicate = NSPredicate(format: "word MATCHES[c]'\(pattern)' AND thm MATCHES[c]'\(valueThem)' ")
//                predicate1 = NSPredicate(format:"ex MATCHES[c]'\(pattern1)' AND thm MATCHES[c]'\(valueThem)'")
//                predicatesin = NSPredicate(format:"sin MATCHES[c]'\(patternsin)'")
//                
//                //                 predicate = NSPredicate(format:"ex MATCHES[c]'\(pattern)'")
//            } else if  (rusAlphabet.contains(searchText.first!)){
//                predicate = NSPredicate(format:"tnl MATCHES[c]'\(pattern)' AND thm MATCHES[c]'\(valueThem)'")
//                //                  predicate = NSPredicate(format:"ex MATCHES[c]'\(pattern)'")
//                predicate1 = NSPredicate(format:"ex MATCHES[c]'\(pattern1)' AND thm MATCHES[c]'\(valueThem)'")
//                predicatesin = NSPredicate(format:"sin MATCHES[c]'\(patternsin)'")
//            }
//            
//            
//            
//            
//            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Words")
//            let fetchRequest1 = NSFetchRequest<NSFetchRequestResult>(entityName: "Words")
//            let fetchRequestSin = NSFetchRequest<NSFetchRequestResult>(entityName: "Words")
//            fetchRequest.predicate = predicate
//            fetchRequest1.predicate = predicate1
//            fetchRequestSin.predicate = predicatesin
//            
//            
//           
//            
////            let request: NSFetchRequest<Words> = Words.fetchRequest()
////            
////            let sort1 = NSSortDescriptor(key: "thm", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
////            let sort2 = NSSortDescriptor(key: "word", ascending: true)
////            
////            request.sortDescriptors = [sort1,sort2]
//            do {
//                itemName = try context.fetch(fetchRequest) as! [NSManagedObject]
//                itemNameSin  = try context.fetch(fetchRequestSin) as! [NSManagedObject]
//                itemName1 = try context.fetch(fetchRequest1) as! [NSManagedObject]
//                itemNameSin.forEach { (item1) in
//                    itemName.append(item1)
//                }
//                itemName1.forEach { (item) in
//                    itemName.append(item)
//                }            }
//            catch{
//                print("could not get data search")
//            }
//            
//            var words = itemName as! [Words]
//           var exist = false
//            newWords.removeAll()
//            for i in 0..<words.count{
//                if(words[i].thm == valueThem){
//                    newWords.append(words[i])
//                }else {
//                    for ii in 0..<newWords.count {
//                        //                        print("i\(i)")
//                        //                        print("ii\(ii)")
//                        if words[i].objectID == newWords[ii].objectID {
//                            exist = true
//                            break
//                        }
//                    }
//                }
//            }
//            //Удаление повторяющегося элемента из массива
//            //            var words = itemName as! [Words]
//            //            var exist = false
//           // newWords.removeAll()
////            for i in 0..<words.count {
////                if newWords.count == 0 {
////                    newWords.append(words[i])
////                } else {
////                    for ii in 0..<newWords.count {
////                        //                        print("i\(i)")
////                        //                        print("ii\(ii)")
////                        if words[i].word == newWords[ii].word {
////                            exist = true
////                            break
////                        }
////                    }
////                    if(!exist){
////                        newWords.append(words[i])
////                    }else{
////                        exist = !exist
////                    }
////                }
////            }
//            
//        }
//        tableView.reloadData()
//        
//        
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.searchBar.endEditing(true)
        self.searchBar.endEditing(true)
    }
    
}


