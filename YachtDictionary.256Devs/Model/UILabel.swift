//
//  UILabel.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 05.10.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit

extension UILabel{
    var optimalHeight: CGFloat{
        get{
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
            label.font =  self.font
            label.text = self.text
            label.sizeToFit()
            
            return label.frame.height
        }
    }
}
