//
//  Section.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 27.08.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import Foundation
struct Section {
    var word: String!
    var name: [String]!
    var transcription:[String]!
    
   var expandet:Bool!
    
    init(word:String,name:[String],transcription:[String], expandet:Bool) {
        self.word = word
        self.name = name
        self.transcription = transcription
        self.expandet = expandet
    }
}

