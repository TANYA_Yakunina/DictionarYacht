//
//  firebase.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 07.09.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import Foundation
import Firebase
var ref: DatabaseReference!

class Firebase: NSObject {
class func GetFireBase() {
    ref = Database.database().reference()
    ref.child("words").observeSingleEvent(of: .value, with: { (snapshot) in
        var vals = Array<String>()
        var sins = Array<String>()
        var exs = Array<String>()
        // Get user value
        for snap in snapshot.children{
            let  snap = snap as? DataSnapshot
            let word = snap?.key
            let desc = snap?.childSnapshot(forPath: "tnc").value as? String
            let transc = snap?.childSnapshot(forPath: "tnl").value as? String
            let theme = snap?.childSnapshot(forPath: "thm").value as? String
            let sinn = snap?.childSnapshot(forPath: "sin").value as? String
            let ex = snap?.childSnapshot(forPath: "ex").value as? String
            for val in (snap?.childSnapshot(forPath: "val").children)!{
                let  val = val as? DataSnapshot
                vals.append((val?.value as? String)!)
            }
            for sin in (snap?.childSnapshot(forPath: "sin").children)!{
                let  sin = sin as? DataSnapshot
                sins.append((sin?.value as? String)!)
            }
            for ex in (snap?.childSnapshot(forPath: "ex").children)!{
                let  ex = ex as? DataSnapshot
                exs.append((ex?.value as? String)!)
            }
            vals.forEach({ (it) in
                print("Value   = \(it)")
            })
            sins.forEach({ (A) in
                print("Sinonium  =  \(A)")
            })
            exs.forEach({ (ex) in
                print("Ex   = \(ex)")
            })
            print("Word  = \(word!)")
            print("sin  = \(sinn)")
            print("Desc  = \(desc)")
            print("Transc  = \(transc)")
            print("Theme  = \(theme)")
    
        }
        
    })
}
}
