//
//  ExpandableHeaderView.swift
//  YachtDictionary.256Devs
//
//  Created by Konstantin Chukhas on 27.08.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//

import UIKit

protocol ExpandableHeaderViewDelegate {
    func toggleFunction(header:ExpandableHeaderView,section:Int)
}


class ExpandableHeaderView: UITableViewHeaderFooterView {

    var delegate: ExpandableHeaderViewDelegate!
    var section:Int!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectHeaderAction)))
    }
    @objc func selectHeaderAction(gestureRecognizer:UITapGestureRecognizer )  {
        let cell = gestureRecognizer.view as! ExpandableHeaderView
        delegate.toggleFunction(header: self, section: cell.section)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func customInit(title:String,section:Int,delegate:ExpandableHeaderViewDelegate){
        self.textLabel?.text = title
        self.section = section
        self.delegate = delegate
     }
    
    override func layoutSubviews() {
        super .layoutSubviews()
        self.textLabel?.textColor = UIColor.black
        
    }
}
