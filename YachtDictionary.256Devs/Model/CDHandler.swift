//
//  CDHandler.swift
//  YachtDictionary.256Devs
//
//  Created by Kostyantin Chukhas on 23.08.2018.
//  Copyright © 2018 Kostyantin Chukhas. All rights reserved.
//
import CoreData
import Foundation

class CDHandler: NSObject {
    //func возвращает  контекст обьекта
    private class func getContext()  -> NSManagedObjectContext{
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    class func saveObject(ex:String,sin:String,thm:String,tnc:String,tnl:String,word:String ) -> Bool {
        let contex = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Words", in: contex)
        //создаем обьект
        let managedObject = NSManagedObject(entity: entity!, insertInto: contex)
        managedObject.setValue(ex, forKey: "ex")
        managedObject.setValue(sin, forKey: "sin")
        managedObject.setValue(thm, forKey: "thm")
        managedObject.setValue(tnc, forKey: "tnc")
        managedObject.setValue(tnl, forKey: "tnl")
        managedObject.setValue(word, forKey: "word")
       
        //устанавлем значение по ключу
        
      
       
        
        do{
            try contex.save()
            return true
        }catch{
            return false
        }
    }
    
    //получаем обьект с нашей базы
    
    class func fetchObject()  -> [Words]?{
        let context = getContext()
        var words:[Words]? = nil
        do{
            words = try context.fetch(Words.fetchRequest())
            return words
        }catch{
            return words 
        }
        
    
    
    }
    
    ///delete all the data in core data
    class func cleanCoreData() {
        
        let fetchRequest:NSFetchRequest<Words> = Words.fetchRequest()
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        
        do {
            print("deleting all contents")
            try getContext().execute(deleteRequest)
        }catch {
            print(error.localizedDescription)
        }
        
    
}
}
